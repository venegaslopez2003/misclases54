/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author Jesus Alberto Venegas Lopez 
 */
public class NotaVenta {
    private int numero;
    private String NombreCliente;
    private Fecha fechaVenta;
    private String  concepto;
    private float total;
    private int tipo;

    public NotaVenta() {
        this.numero = 0;
        this.NombreCliente = "";
        this.fechaVenta = new Fecha();
        this.concepto = "";
        this.total = 0.0f;
        this.tipo = 0;
        
    }

    public NotaVenta(int numero, String NombreCliente, Fecha fechaVenta, String concepto, float total, int tipo) {
        this.numero = numero;
        this.NombreCliente = NombreCliente;
        this.fechaVenta = fechaVenta;
        this.concepto = concepto;
        this.total = total;
        this.tipo = tipo;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNombreCliente() {
        return NombreCliente;
    }

    public void setNombreCliente(String NombreCliente) {
        this.NombreCliente = NombreCliente;
    }

    public Fecha getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(Fecha fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
    
    
     public float calcularImpuesto(){
        float impuesto =0.0f;
        impuesto= this.total*.16f;
        return impuesto;
            
    }   
     
     public float calcularTotal(){
        float totalPagar=0.0f;
        totalPagar=this.total + this.calcularImpuesto();
        return totalPagar;
    }
    
    
    
    
    
    
    
    
    
    
    
}
