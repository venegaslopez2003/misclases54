/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author Jesus Alberto
 */
public class Factura {
    private int numFactura;
    private String rfc;
    private String nombreCliente;
    private String domicilio;
    private String descripcion;
    private String fecha;
    private float totalVenta;

    public Factura() {
        this.numFactura=0;
        this.rfc="";
        this.nombreCliente="";
        this.domicilio="";
        this.descripcion="";
        this.fecha="";
        this.totalVenta=0.0f;
        
        
    }

    public Factura(int numFactura, String rfc, String nombreCliente, String domicilio, String descripcion, String Fecha, float totalVenta) {
        this.numFactura = numFactura;
        this.rfc = rfc;
        this.nombreCliente = nombreCliente;
        this.domicilio = domicilio;
        this.descripcion = descripcion;
        this.fecha = Fecha;
        this.totalVenta = totalVenta;
    }
    public Factura(Factura otro) {
        this.numFactura = this.numFactura;
        this.rfc = this.rfc;
        this.nombreCliente = this.nombreCliente;
        this.domicilio = this.domicilio;
        this.descripcion = this.descripcion;
        this.fecha = this.fecha;
        this.totalVenta = this.totalVenta;

   
    }

    public int getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(int numFactura) {
        this.numFactura = numFactura;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String Fecha) {
        this.fecha = Fecha;
    }

    public float getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(float totalVenta) {
        this.totalVenta = totalVenta;
    }
    
    public float calcularImpuesto(){
        float impuesto =0.0f;
        impuesto= this.totalVenta*.16f;
        return impuesto;
            
    }   
    
    
    public float calcularTotal(){
        float total=0.0f;
        total=this.totalVenta + this.calcularImpuesto();
        return total;
    }
    public void imprimirFactura(){
        System.out.println("Numero de Factura:"+this.numFactura);
        System.out.println("RFC :"+this.rfc);
        System.out.println("Nombre de cliente:"+this.nombreCliente);
        System.out.println("Domicilio :"+this.domicilio);
        System.out.println("Descripcion:"+this.descripcion);
        System.out.println("La fecha es:"+this.fecha);
        System.out.println(" venta ="+this.totalVenta);
        System.out.println(" Impuesto="+this.calcularImpuesto());
        System.out.println(" El total de venta ="+this.calcularTotal());
    }
        
        
    
}
   

    
    
    
    

    


