/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author Janis
 */
public class Nomina {
    private int numNomina;
    private String nombre;
    private int nivel;
    private int pagoHora;
    private int horaTrabajada;

    public Nomina() {
       
    }

    public Nomina(int numNomina, String nombre, int nivel, int pagoHora, int horaTrabajada) {
        this.numNomina = numNomina;
        this.nombre = nombre;
        this.nivel = nivel;
        this.pagoHora = pagoHora;
        this.horaTrabajada = horaTrabajada;
    }

    public int getNumNomina() {
        return numNomina;
    }

    public void setNumNomina(int numNomina) {
        this.numNomina = numNomina;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(int pagoHora) {
        this.pagoHora = pagoHora;
    }

    public int getHoraTrabajada() {
        return horaTrabajada;
    }

    public void setHoraTrabajada(int horaTrabajada) {
        this.horaTrabajada = horaTrabajada;
    }
    
    
    
    
    
    


    
    
            
    
    
}
